#!/usr/bin/python
#-*- coding: utf-8 -*-
from tkinter import *
import math

def programme():
    prog = Toplevel()
    prog.title("Vos tricots :: aide")
    text = Message(prog, text="""Voici ce que vous devez savoir sur le programme.

-Saisie des options, mailles et centimètres-
    Afin d'éviter que le programme ne génére des erreurs, vous devez saisir des chiffres au clavier, et uniquement des chiffres. Pour les calculs, si vous entrez des chiffres à virgules (comme 19,5), sachez qu'en informatique il ne faut par mettre de virgule mais un point, ainsi le chiffre de l'exemple ci-dessus devra être écrit 19.5.

-Calcul des mailles-
    Le calcul des mailles vous permet de déterminer le nombre de mailles que vous devrez tricotter afin d'atteindre la longueur souhaitée en vous basant sur le nombre de mailles de votre échantillon, la longueur en centimètres de votre échantillon et la longueur souhaitée pour votre tricot.

-Calcul des centimètres-
    Le calcul des centimètres vous permet de déterminer la longueur de tricot que vous devrez tricotter afin d'atteindre le nombre de mailles souhaitées en vous basant sur le nombre de mailles de votre échantillon, la longueur en centimètres de votre échantillon et le nombre de mailles souhaitées pour votre tricot.""")
    text.pack()
    bou1 = Button(prog, text="Fermer les explication", fg="darkred", command=prog.destroy)
    bou1.pack()
    prog.mainloop()

def mailles():
    global m1, c1, c2, reponse

    def calcmailles():
        if m1.get()=="" or c1.get()=="" or c2.get()=="":
            reponse['text']="L'un de vos champs est vide, calcul impossible."
            reponse['fg']="red"
        else:
            calcul = "("+str(m1.get())+"*"+str(c2.get())+")/"+str(c1.get())
            reponse['text']="Il vous faut une longueur de "+str(float(eval(calcul)))+" mailles."
            reponse['background']="#ffffff"
            reponse['fg']="black"

    mail = Toplevel()
    mail.title("Vos tricots :: mailles")
    txtm1 = Label(mail, text="Nombre de mailles :", fg="dark blue")
    txtc1 = Label(mail, text="Nombre de centimètres :", fg="dark blue")
    txtc2 = Label(mail, text="Nombre de centimètres souhaités :", fg="dark blue")
    m1 = StringVar()
    entm1 = Entry(mail, textvariable = m1)
    c1 = StringVar()
    entc1 = Entry(mail, textvariable = c1)
    c2 = StringVar()
    entc2 = Entry(mail, textvariable = c2)
    reponse = Label(mail, text="En attente des valeurs.", fg="darkblue")
    bou1 = Button(mail, text="Calculer les mailles", fg="darkgreen", command=calcmailles)
    bou2 = Button(mail, text="Quitter le calcul", fg="darkred", command=mail.destroy)
    txtm1.grid(row=1, sticky=E)
    txtc1.grid(row=2, sticky=E)
    txtc2.grid(row=3, sticky=E)
    entm1.grid(row=1, column=2)
    entc1.grid(row=2, column=2)
    entc2.grid(row=3, column=2)
    bou1.grid(row=4, sticky=SE)
    bou2.grid(row=4, column=2)
    reponse.grid(row=5, columnspan=3)
    mail.mainloop()

def centimetres():
    global m1, c1, m2, reponse

    def calccenti():
        if m1.get()=="" or c1.get()=="" or m2.get()=="":
            reponse['text']="L'un de vos champs est vide, calcul impossible."
            reponse['fg']="red"
        else:
            calcul = "("+str(m2.get())+"*"+str(c1.get())+")/"+str(m1.get())
            reponse['text']="Il vous faut une longueur de "+str(float(eval(calcul)))+" centimètres."
            reponse['background']="#ffffff"
            reponse['fg']="black"

    mail = Toplevel()
    mail.title("Vos tricots :: centimètres")
    txtm1 = Label(mail, text="Nombre de mailles :", fg="dark blue")
    txtc1 = Label(mail, text="Nombre de centimètres :", fg="dark blue")
    txtm2 = Label(mail, text="Nombre de mailles souhaitées :", fg="dark blue")
    m1 = StringVar()
    entm1 = Entry(mail, textvariable = m1)
    c1 = StringVar()
    entc1 = Entry(mail, textvariable = c1)
    m2 = StringVar()
    entm2 = Entry(mail, textvariable = m2)
    reponse = Label(mail, text="En attente des valeurs.", fg="darkblue")
    bou1 = Button(mail, text="Calculer les centimètres", fg="darkgreen", command=calccenti)
    bou2 = Button(mail, text="Quitter le calcul", fg="darkred", command=mail.destroy)
    txtm1.grid(row=1, sticky=E)
    txtc1.grid(row=2, sticky=E)
    txtm2.grid(row=3, sticky=E)
    entm1.grid(row=1, column=2)
    entc1.grid(row=2, column=2)
    entm2.grid(row=3, column=2)
    bou1.grid(row=4, sticky=SE)
    bou2.grid(row=4, column=2)
    reponse.grid(row=5, columnspan=3)
    mail.mainloop()
    
    m1=eval(input("Saisir le nombre de mailles au clavier : "))
    m1=float(m1)
    c1=eval(input("Saisir le nombre de centimètres au clavier : "))
    c1=float(c1)
    m2=eval(input("Saisir le nombre de mailles souhaités au clavier : "))
    m2=float(m2)
    c2=c1*m2
    c2=c2/m1
    print("\n    >>  Il vous faut une longueur de",c2,"centimètres.\n")

menu = Tk()
menu.title("Vos tricots")
can1=Label(menu, text="Choisir une option pour le programme :", background="#ffffff")
can1.pack()
bou1=Button(menu, text="Lire les explications sur le programme", fg="dark blue", command=programme)
bou1.pack()
bou2=Button(menu, text="Calculer le nombre de mailles", fg="dark blue", command=mailles)
bou2.pack()
bou3=Button(menu, text="Calculer le nombre de centimètres", fg="darkblue", command=centimetres)
bou3.pack()
bou4=Button(menu, text="Quitter le programme", fg="darkred", command=menu.destroy)
bou4.pack()
menu.mainloop()
